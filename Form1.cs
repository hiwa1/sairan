﻿/*
 * Project:     Serial Port Interface with C#
 * Company:     StudentCompanion
 * Created:     April 2013
 * Updated:    February 2020
 * Visual Studio: 2019
 * .NET Framework: 4.6.2
 * Notes:       This is a simple demontration on how to use the SerialPort control for
 *              communicating with your PC's  COM Port. 
 *              This is for educational purposes only and not for any commercial use. 
 *              For more on this, please visit: https://www.studentcompanion.co.za/creating-a-serial-port-interface-with-c/
 * */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;  //This is a namespace that contains the SerialPort class
using System.Globalization;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using RandomSolutions;

namespace Serial_Communication
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public List<byte> Received = new List<byte>();
        public List<Data> ExcelData = new List<Data>();
        public StringBuilder AllText { get; set; } = new StringBuilder();
        public Data TempData = new Data();
        int processed = 0;
        bool Break = false;
        byte a = 0xaf;
        byte b = 0xfa;

        int CorrectCount = 0;
        int InCorrectCount = 0;
        private void Form1_Load(object sender, EventArgs e)
        {



            CheckForIllegalCrossThreadCalls = false;
            cmbBaudRate.SelectedIndex = 7;
            cmbParity.SelectedIndex = 0;

            timer1.Interval = 1;



        }



        private void UpdatePorts()
        {
            // Retrieve the list of all COM ports on your Computer
            cmbPortName.Items.Clear();
            string[] ports = SerialPort.GetPortNames().GroupBy(p => p).Select(p => p.Key).OrderBy(p => p).ToArray();
            foreach (string port in ports)
            {
                cmbPortName.Items.Add(port);
            }
        }
        private SerialPort ComPort = new SerialPort();

        public int Counter { get; private set; } = 0;
        public bool ExcelMode { get; private set; } = false;

        private void connect()
        {
            //byte[] bytes = new byte[] { 0xaf, 0xfa, 0x00, 0x00 ,0x00 ,0x00 ,0x00 ,0xd4 ,0xd3 };
            //MessageBox.Show(((int)CheckSum(bytes, 9)).ToString());


            bool error = false;



            if (cmbPortName.SelectedIndex != -1 & cmbBaudRate.SelectedIndex != -1 & cmbParity.SelectedIndex != -1 /*& cmbDataBits.SelectedIndex != -1 & cmbStopBits.SelectedIndex != -1*/)
            {

                ComPort.PortName = cmbPortName.Text;
                ComPort.BaudRate = int.Parse(cmbBaudRate.Text);
                ComPort.Parity = (Parity)Enum.Parse(typeof(Parity), cmbParity.Text);
                ComPort.DataBits = 8;
                ComPort.StopBits = StopBits.One;

                try
                {

                    ComPort.Open();
                    ComPort.DataReceived += SerialPortDataReceived;
                }
                catch (UnauthorizedAccessException) { error = true; }
                catch (System.IO.IOException) { error = true; }
                catch (ArgumentException) { error = true; }

                if (error) MessageBox.Show(this, "Could not open the COM port. Most likely it is already in use, has been removed, or is unavailable.", "COM Port unavailable", MessageBoxButtons.OK, MessageBoxIcon.Stop);

            }
            else
            {
                MessageBox.Show("Please select all the COM Serial Port Settings", "Serial Port Interface", MessageBoxButtons.OK, MessageBoxIcon.Stop);

            }
            if (ComPort.IsOpen)
            {
                btnConnect.Text = "Disconnect";
                btnSend.Enabled = true;
                if (!rdText.Checked & !rdHex.Checked)
                {
                    rdText.Checked = true;
                }
                groupBox1.Enabled = false;
            }
        }

        private void disconnect()
        {
            ComPort.Close();
            btnConnect.Text = "Connect";
            btnSend.Enabled = false;
            groupBox1.Enabled = true;

        }
        private void btnConnect_Click(object sender, EventArgs e)

        {
            if (ComPort.IsOpen)
            {
                disconnect();
            }
            else
            {
                connect();
                if (!backgroundWorker1.IsBusy)
                    backgroundWorker1.RunWorkerAsync();
            }

            //System.Threading.Timer timer = new System.Threading.Timer(_ => MainWhile(), null, 0,1);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            var ctrls = this.Controls.OfType<Label>().ToList();
            var lbls = ctrls.Where(p => p.Name.Contains("lbl")).ToList();
            lbls.ForEach(p => p.Text = "---");
            rtxtDataArea.Clear();
            txtSend.Clear();
            processed = 0;
            Break = false;
            Received.Clear();
            CorrectCount = 0;
            InCorrectCount = 0;
            lblCorrect.Text = CorrectCount.ToString();
            lblIncorrect.Text = InCorrectCount.ToString();
            lblReceivedBytes.Text = "0";
            ExcelData.Clear();
            AllText.Clear();
        }

        private void sendData()
        {
            bool error = false;
            if (rdText.Checked == true)
            {
                ComPort.Write(txtSend.Text);
                rtxtDataArea.ForeColor = Color.Green;
                txtSend.Clear();

            }
            else
            {
                try
                {
                    byte[] data = HexStringToByteArray(txtSend.Text);
                    ComPort.Write(data, 0, data.Length);
                    rtxtDataArea.ForeColor = Color.Blue;
                    rtxtDataArea.AppendText(txtSend.Text.ToUpper() + "\n");
                    txtSend.Clear();
                }
                catch (FormatException) { error = true; }
                catch (ArgumentException) { error = true; }

                if (error) MessageBox.Show(this, "Not properly formatted hex string: " + txtSend.Text + "\n" + "example: E1 FF 1B", "Format Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);

            }
        }

        private byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            sendData();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ComPort.IsOpen) ComPort.Close();
        }

        delegate void SetTextCallback(string text);
        private void SetText(string text)
        {
            if (this.rtxtDataArea.InvokeRequired)
            {
                rtxtDataArea.ForeColor = Color.Green;

                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.rtxtDataArea.AppendText(text);
            }
        }
        private void SerialPortDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            GetData(sender);
            //CheckForHeader();

        }

        private void Extract()
        {
            if (Received.Count - processed > 1)
            {
                if (!(Received[processed] == a && Received[processed + 1] == b))
                    processed++;
                else if (Received.Count - processed > 9)
                {
                    var data = Received.GetRange(processed, 10);

                    if (CheckSum(data.ToArray(), 9) != (byte)data[9])
                    {
                        InCorrectCount++;
                        lblIncorrect.Text = InCorrectCount.ToString();
                        if (!chkCrc.Checked)
                            Analyze(data);
                    }
                    else
                    {
                        CorrectCount++;
                        lblCorrect.Text = CorrectCount.ToString();
                        Analyze(data);
                    }
                    processed += 10;
                }
            }

        }

        private void CheckForHeader()
        {
            if (Received.Contains(a) && !Break)
            {
                processed = Received.IndexOf(a);
                Break = true;
            }

        }

        private void GetData(object sender)
        {
            var serialPort = (SerialPort)sender;
            if (rdText.Checked)
            {
                var txt = serialPort.ReadExisting();
                rtxtDataArea.Text += txt;
                try
                {
                    var i = txt.LastIndexOf("0d");
                    if (i != -1)
                    {
                        string lastIndex = txt.Substring(0, i);
                        var j = lastIndex.LastIndexOf("0d");
                        if (j != -1)
                        {
                            var last = lastIndex.Substring(j + 2);
                            lblLastLine.Text = last;
                        }
                        else
                            lblLastLine.Text = lastIndex;
                    }
                }
                catch
                {

                }
            }
            else
            {
                int length = serialPort.BytesToRead;
                var buf = new byte[length];
                serialPort.Read(buf, 0, length);
                Received.AddRange(buf.ToList());
                lblReceivedBytes.Text = Received.Count.ToString();
            }
        }
        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}" + " ", b);
            return hex.ToString();
        }

        private void Analyze(List<byte> data)
        {
            var dataText = ByteArrayToString(data.ToArray());
            TempData.RawData = dataText;
            if (!ExcelMode)
                AllText.Append(dataText + "\r\n");
            if (saveAfter)
            {
                StreamWriter sw = new StreamWriter(saveAfterFile, true);
                sw.Write(dataText + "\r\n");
                sw.Close();
            }

            if (Counter % 49 == 0 || ExcelMode)
            {
                var rng = data.GetRange(2, 2);
                byte[] dobyte = new byte[] { rng[1], rng[0] };
                var distance = BitConverter.ToInt16(dobyte.ToArray(), 0);
                TempData.Distance = distance.ToString();
                var rpw = data[4];
                var returnReceive = data[5];
                var counterStatus = data[6];
                var Data = data[7];
                var PacketNo = data[8];


                Return5(returnReceive);
                RPW(rpw);
                Distance(distance);
                CounterStatus3(counterStatus);
                PacketCounter(PacketNo);
                DataPacket4(Data);
                if (!ExcelMode)
                {
                    lblLastLine.Text = dataText;
                    rtxtDataArea.AppendText(dataText + "\r\n");
                }

            }
            Counter++;
        }


        private void Analyze2(List<byte> data)
        {
            var dataText = ByteArrayToString(data.ToArray());
            TempData.RawData = dataText;

            var rng = data.GetRange(2, 2);
            byte[] dobyte = new byte[] { rng[1], rng[0] };
            var distance = BitConverter.ToInt16(dobyte.ToArray(), 0);
            TempData.Distance = distance.ToString();
            var rpw = data[4];
            var returnReceive = data[5];
            var counterStatus = data[6];
            var Data = data[7];
            var PacketNo = data[8];


            Return5_1(returnReceive);
            RPW_1(rpw);
            //Distance_1(distance);
            CounterStatus3_1(counterStatus);
            PacketCounter_1(PacketNo);
            DataPacket4_1(Data);



            //Counter++;
        }

        private void PacketCounter(byte packetNo)
        {
            lblPacketNo.Text = packetNo.ToString();
            TempData.PacketCounter = packetNo.ToString();
        }
        private void PacketCounter_1(byte packetNo)
        {
            TempData.PacketCounter = packetNo.ToString();
        }

        private void CounterStatus3(byte counterStatus)
        {
            Pulse(counterStatus);
            NumberOfTargets(counterStatus);
            Counter0(counterStatus);
        }
        private void CounterStatus3_1(byte counterStatus)
        {
            Pulse_1(counterStatus);
            NumberOfTargets_1(counterStatus);
            Counter0_1(counterStatus);
        }

        private void Counter0(byte counterStatus)
        {
            var mode = (byte)(counterStatus & 0x1f);
            lblCounter0.Text = mode.ToString();
            TempData.Counter0 = lblCounter0.Text;
        }
        private void Counter0_1(byte counterStatus)
        {
            var mode = (byte)(counterStatus & 0x1f);
            TempData.Counter0 = mode.ToString();
        }

        private void NumberOfTargets(byte counterStatus)
        {
            var mode = (byte)(counterStatus & 0x60);
            if (mode == 0x00)
                lblNumberOfTarget.Text = "No Target";
            else if (mode == 0x20)
                lblNumberOfTarget.Text = "1 Target";
            else if (mode == 0x40)
                lblNumberOfTarget.Text = "2 Target";
            else if (mode == 0x60)
                lblNumberOfTarget.Text = "3 Or More Target";
            TempData.NumberOfTargets = lblNumberOfTarget.Text;
        }
        private void NumberOfTargets_1(byte counterStatus)
        {
            var mode = (byte)(counterStatus & 0x60);
            if (mode == 0x00)
                TempData.NumberOfTargets = "No Target";
            else if (mode == 0x20)
                TempData.NumberOfTargets = "1 Target";
            else if (mode == 0x40)
                TempData.NumberOfTargets = "2 Target";
            else if (mode == 0x60)
                TempData.NumberOfTargets = "3 Or More Target";

        }

        private void Pulse(byte counterStatus)
        {
            var mode = (byte)(counterStatus & 0x80);
            if (mode == 0x80)
                lblPulse.Text = "D";
            else
                lblPulse.Text = "R";
            TempData.Pulse = lblPulse.Text;
        }
        private void Pulse_1(byte counterStatus)
        {
            var mode = (byte)(counterStatus & 0x80);
            if (mode == 0x80)
                TempData.Pulse = "D";
            else
                TempData.Pulse = "R";

        }

        private void Distance(short distance)
        {
            lblDistance.Text = distance.ToString();
        }
        private void Distance_1(short distance)
        {
            //lblDistance.Text = distance.ToString();
        }

        private void RPW(byte rpw)
        {
            lblRPW.Text = rpw.ToString();
            TempData.RPW = rpw.ToString();
        }
        private void RPW_1(byte rpw)
        {
            TempData.RPW = rpw.ToString();
        }

        private void DataPacket4(byte Data)
        {
            byte Thirty = 0x1e;
            byte two = 0x02;
            byte mask = 0xc0;


            byte masked = (byte)(Data & mask);
            byte pureData = (byte)(Data & 0x3f);
            if (masked == (byte)0)
            {
                byte tmp2 = (byte)(pureData * two);
                var t = (tmp2 - Thirty).ToString();
                lblTemperature.Text = t;
                TempData.Temperature = t;
            }
            else if (masked == (byte)0x40)
            {
                byte pitch = (byte)(pureData * two);
                var p = pitch.ToString();
                lblPitch.Text = p;
                TempData.Pitch = p;
            }
            else if (masked == (byte)0x80)
            {
                byte roll = (byte)(pureData);
                var r = roll.ToString();
                lblRoll.Text = r;
                TempData.Roll = r;
            }
            else if (masked == (byte)0xC0)
            {
                byte gate = (byte)(pureData);
                if ((lblRecMode.Text == "T" || lblRecMode.Text == "F") && lblRecCommand.Text == "D")
                {
                    lblSpeed.Text = (gate * 0x1e).ToString();
                    TempData.Speed = lblSpeed.Text;
                }
                else
                {
                    var g = gate.ToString();
                    TempData.Gate = g;
                    lblGate.Text = g;
                }
            }

        }
        private void DataPacket4_1(byte Data)
        {
            byte Thirty = 0x1e;
            byte two = 0x02;
            byte mask = 0xc0;


            byte masked = (byte)(Data & mask);
            byte pureData = (byte)(Data & 0x3f);
            if (masked == (byte)0)
            {
                byte tmp2 = (byte)(pureData * two);
                var t = (tmp2 - Thirty).ToString();
                TempData.Temperature = t;
            }
            else if (masked == (byte)0x40)
            {
                byte pitch = (byte)(pureData * two);
                var p = pitch.ToString();
                TempData.Pitch = p;
            }
            else if (masked == (byte)0x80)
            {
                byte roll = (byte)(pureData);
                var r = roll.ToString();
                TempData.Roll = r;
            }
            else if (masked == (byte)0xC0)
            {
                byte gate = (byte)(pureData);

                //if ((lblRecMode.Text == "T" || lblRecMode.Text == "F") && lblRecCommand.Text == "D")
                if ((TempData.Mode == "T" || TempData.Mode == "F") && TempData.RecCommand == "D")
                {
                    lblSpeed.Text = (gate * 0x1e).ToString();
                    TempData.Speed = lblSpeed.Text;
                }
                else
                    TempData.Gate = gate.ToString();
            }

        }

        private void Return5(byte returnReceive)
        {
            RecMode(returnReceive);
            RecCommand(returnReceive);
            Mode(returnReceive);
            Command(returnReceive);
            Counter1(returnReceive);
        }
        private void Return5_1(byte returnReceive)
        {
            RecMode_1(returnReceive);
            RecCommand_1(returnReceive);
            Mode_1(returnReceive);
            Command_1(returnReceive);
            Counter1_1(returnReceive);
        }

        private void Counter1(byte returnReceive)
        {
            var mode = (byte)(returnReceive & 0x03);
            lblCounter1.Text = mode.ToString();
            TempData.Counter1 = mode.ToString();
        }
        private void Counter1_1(byte returnReceive)
        {
            var mode = (byte)(returnReceive & 0x03);
            TempData.Counter1 = mode.ToString();
        }

        private void Command(byte returnReceive)
        {
            var mode = (byte)(returnReceive & 0x04);
            if (mode == 0x04)
                lblCommand.Text = "D";
            else
                lblCommand.Text = "U";
            TempData.Command = lblCommand.Text;
        }
        private void Command_1(byte returnReceive)
        {
            var mode = (byte)(returnReceive & 0x04);
            if (mode == 0x04)
                TempData.Command = "D";
            else
                TempData.Command = "U";
        }

        private void Mode(byte returnReceive)
        {
            var mode = (byte)(returnReceive & 0x18);
            if (mode == 0x00)
                lblMode.Text = "S Or I";
            else if (mode == 0x08)
                lblMode.Text = "P";
            else if (mode == 0x10)
                lblMode.Text = "T";
            else if (mode == 0x18)
                lblMode.Text = "F";
            TempData.Mode = lblMode.Text;
        }
        private void Mode_1(byte returnReceive)
        {
            var mode = (byte)(returnReceive & 0x18);
            if (mode == 0x00)
                TempData.Mode = "S Or I";
            else if (mode == 0x08)
                TempData.Mode = "P";
            else if (mode == 0x10)
                TempData.Mode = "T";
            else if (mode == 0x18)
                TempData.Mode = "F";
        }

        private void RecCommand(byte returnReceive)
        {
            var mode = (byte)(returnReceive & 0x20);
            if (mode == 0x20)
                lblRecCommand.Text = "D";
            else
                lblRecCommand.Text = "U";
            TempData.RecCommand = lblRecCommand.Text;
        }
        private void RecCommand_1(byte returnReceive)
        {
            var mode = (byte)(returnReceive & 0x20);
            if (mode == 0x20)
                TempData.RecCommand = "D";
            else
                TempData.RecCommand = "U";
        }

        private void RecMode(byte returnReceive)
        {
            var mode = (byte)(returnReceive & 0xc0);
            if (mode == 0x00)
                lblRecMode.Text = "S Or I";
            else if (mode == 0x40)
                lblRecMode.Text = "P";
            else if (mode == 0x80)
                lblRecMode.Text = "T";
            else if (mode == 0xC0)
                lblRecMode.Text = "F";
            TempData.RecMode = lblRecMode.Text;
        }
        private void RecMode_1(byte returnReceive)
        {
            var mode = (byte)(returnReceive & 0xc0);
            if (mode == 0x00)
                TempData.RecMode = "S Or I";
            else if (mode == 0x40)
                TempData.RecMode = "P";
            else if (mode == 0x80)
                TempData.RecMode = "T";
            else if (mode == 0xC0)
                TempData.RecMode = "F";
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            UpdatePorts();
        }

        public byte CheckSum(byte[] input, int bytes)
        {
            //return input.Aggregate<byte, byte>(0, (current, b) => (byte)((current + b) & 0xff));
            byte generator = 0xD5;
            byte CRC_Tele = 0;
            byte i = 0, k = 0;

            for (k = 0; k < bytes; k++)
            {
                CRC_Tele ^= (input[k]);
                for (i = 0; i < 8; i++)
                {
                    if ((CRC_Tele & 0x80) != 0)
                    {
                        CRC_Tele = (byte)((CRC_Tele << 1) ^ generator);
                    }
                    else
                    {
                        CRC_Tele <<= 1;
                    }
                }
            }
            return CRC_Tele;

        }

        private void cmbPortName_DropDown(object sender, EventArgs e)
        {
            UpdatePorts();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            MainWhile();
        }

        private void MainWhile()
        {
            while (true)
            {
                Application.DoEvents();
                //    if (Break)
                Extract();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog op = new SaveFileDialog();
            op.Filter = "Text files(*.txt)| *.txt";
            DialogResult res = op.ShowDialog();
            if (res == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(op.FileName, false);
                sw.Write(AllText);
                sw.Close();
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {

            await ExcelExport();

        }

        private async Task ExcelExport()
        {
            Clear();
            ExcelData = new List<Data>();
            OpenFileDialog op = new OpenFileDialog();
            op.Filter = "Text files(*.txt)| *.txt";
            DialogResult res = op.ShowDialog();
            if (res == DialogResult.OK)
            {
                try
                {
                    await SaveTextAsync(op.FileName);
                    SaveFileDialog sd = new SaveFileDialog();
                    sd.Filter = "Excel files(*.xlsx)| *.xlsx";
                    DialogResult result = sd.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        progressBar1.Visible = true;
                        //progressBar1.Maximum = ExcelData.Count;
                        //progressBar1.Value = 0;
                        await SaveExcel(sd.FileName);
                        progressBar1.Visible = false;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Data is invalid");
                }

            }
        }



        private async Task SaveTextAsync(string FileName)
        {
            await Task.Run(() =>
            {
                StreamReader sr = new StreamReader(FileName);
                var lines = sr.ReadToEnd().Split('\n').ToList();
                sr.Close();
                //while (!sr.EndOfStream)
                var watch = new System.Diagnostics.Stopwatch();






                //watch.Start();
                lines.ForEach(str =>
                {
                    TempData = new Data();
                    //var str = sr.ReadLine().Replace("\r\n", "").Replace(" ", "");
                    str = str.Replace("\r", "").Replace("\n", "").Replace(" ", "");
                    if (str.Length > 5)
                    {
                        var data = StringToByteArray(str);
                        ExcelMode = true;
                        Analyze2(data.ToList());

                        ExcelData.Add(TempData);
                        ExcelMode = false;
                    }

                });
                //watch.Stop();
                //MessageBox.Show(watch.ElapsedMilliseconds.ToString());

            });
        }

        private async Task SaveExcel(string fileName)
        {
            await Task.Run(() =>
            {
                var items = ExcelData.Select(data => new
                {
                    PacketCounter = data.PacketCounter,
                    RawData = data.RawData,
                    Distance = data.Distance,
                    RPW = data.RPW,
                    Counter0 = data.Counter0,
                    Counter1 = data.Counter1,
                    Command = data.Command,
                    Mode = data.Mode,
                    RecCommand = data.RecCommand,
                    RecMode = data.RecMode,
                    NumberOfTargets = data.NumberOfTargets,
                    Pulse = data.Pulse,
                    Temperature = data.Temperature,
                    Pitch = data.Pitch,
                    Roll = data.Roll,
                    Gate = data.Gate,
                    Speed = data.Speed
                });
                byte[] excel = items.ToExcel();
                File.WriteAllBytes(fileName, excel);
                MessageBox.Show("Finished");
                //int rowcount = 0;
                //var excel = new Microsoft.Office.Interop.Excel.Application();
                //excel.Visible = false;
                //excel.DisplayAlerts = false;
                //var worKbooK = excel.Workbooks.Add(Type.Missing);


                //var worKsheeT = (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.ActiveSheet;
                //worKsheeT.Name = "Records";
                //rowcount += 1;
                //worKsheeT.Cells[rowcount, 1] = "PacketCounter";
                //worKsheeT.Cells[rowcount, 2] = "RawData";
                //worKsheeT.Cells[rowcount, 3] = "Distance";
                //worKsheeT.Cells[rowcount, 4] = "RPW";
                //worKsheeT.Cells[rowcount, 5] = "Counter0";
                //worKsheeT.Cells[rowcount, 6] = "Counter1";
                //worKsheeT.Cells[rowcount, 7] = "Command";
                //worKsheeT.Cells[rowcount, 8] = "Mode";
                //worKsheeT.Cells[rowcount, 9] = "RecCommand";
                //worKsheeT.Cells[rowcount, 10] = "RecMode";
                //worKsheeT.Cells[rowcount, 11] = "NumberOfTargets";
                //worKsheeT.Cells[rowcount, 12] = "Pulse";
                //worKsheeT.Cells[rowcount, 13] = "Temperature";
                //worKsheeT.Cells[rowcount, 14] = "Pitch";
                //worKsheeT.Cells[rowcount, 15] = "Roll";
                //worKsheeT.Cells[rowcount, 16] = "Gate";
                //worKsheeT.Cells[rowcount, 17] = "Speed";


                //foreach (var data in ExcelData)
                //{
                //    rowcount += 1;
                //    worKsheeT.Cells[rowcount, 1] = data.PacketCounter;
                //    worKsheeT.Cells[rowcount, 2] = data.RawData;
                //    worKsheeT.Cells[rowcount, 3] = data.Distance;
                //    worKsheeT.Cells[rowcount, 4] = data.RPW;
                //    worKsheeT.Cells[rowcount, 5] = data.Counter0;
                //    worKsheeT.Cells[rowcount, 6] = data.Counter1;
                //    worKsheeT.Cells[rowcount, 7] = data.Command;
                //    worKsheeT.Cells[rowcount, 8] = data.Mode;
                //    worKsheeT.Cells[rowcount, 9] = data.RecCommand;
                //    worKsheeT.Cells[rowcount, 10] = data.RecMode;
                //    worKsheeT.Cells[rowcount, 11] = data.NumberOfTargets;
                //    worKsheeT.Cells[rowcount, 12] = data.Pulse;
                //    worKsheeT.Cells[rowcount, 13] = data.Temperature;
                //    worKsheeT.Cells[rowcount, 14] = data.Pitch;
                //    worKsheeT.Cells[rowcount, 15] = data.Roll;
                //    worKsheeT.Cells[rowcount, 16] = data.Gate;
                //    worKsheeT.Cells[rowcount, 17] = data.Speed;
                //    progressBar1.Value++;
                //}
                //worKbooK.SaveAs(fileName);
                //worKbooK.Close();
                //excel.Quit();
            });
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            MainWhile();
        }
        bool saveAfter = false;
        string saveAfterFile = "";
        private void btnSaveAfter_Click(object sender, EventArgs e)
        {
            if (saveAfter)
            {
                btnSaveAfter.BackColor = System.Drawing.SystemColors.ControlLightLight;
                saveAfter = false;
            }
            else
            {
                SaveFileDialog op = new SaveFileDialog();
                op.Filter = "Text files(*.txt)| *.txt";
                DialogResult res = op.ShowDialog();
                if (res == DialogResult.OK)
                {
                    btnSaveAfter.BackColor = Color.Red;
                    saveAfterFile = op.FileName;
                    saveAfter = true;
                }
            }
        }

        private async void btnReadBinaryFile_ClickAsync(object sender, EventArgs e)
        {
            await ExcelExportFromBinary();
        }

        private async Task ExcelExportFromBinary()
        {
            Clear();
            ExcelData = new List<Data>();
            OpenFileDialog op = new OpenFileDialog();
            op.Filter = "Text files(*.log)| *.log";
            DialogResult res = op.ShowDialog();
            if (res == DialogResult.OK)
            {
                try
                {
                    await SaveBinaryAsync(op.FileName);
                    SaveFileDialog sd = new SaveFileDialog();
                    sd.Filter = "Excel files(*.xlsx)| *.xlsx";
                    DialogResult result = sd.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        progressBar1.Visible = true;
                        //progressBar1.Maximum = ExcelData.Count;
                        //progressBar1.Value = 0;
                        await SaveExcel(sd.FileName);
                        progressBar1.Visible = false;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Data is invalid");
                }

            }
        }
        private IEnumerable<byte[]> Packetize(IEnumerable<byte> stream,byte sep)
        {
            var buffer = new List<byte>();
            foreach (byte b in stream)
            {
                buffer.Add(b);
                if (b == sep)
                {
                    buffer.Remove(b);
                    yield return buffer.ToArray();
                    buffer.Clear();
                }
            }
            if (buffer.Count > 0)
                yield return buffer.ToArray();
        }
        private async Task SaveBinaryAsync(string FileName)
        {
            await Task.Run(() =>
            {
                var bytes = File.ReadAllBytes(FileName);
                //int counter = 0;
                //while(counter < bytes.Length && bytes[counter] != 0xaf)
                //{                                        
                //        counter++;                    
                //}
                //byte[] pureBytes = new byte[bytes.Length - counter];
                //for (int j = counter; j < bytes.Length; j++)
                //{
                //    pureBytes[j-counter] = bytes[j];
                //}

                //var lines = bytes.ToList().GetEnumerableOfEnumerables(10).ToList();
                var lines = Packetize(bytes,175).ToList();

                List<byte[]> newLines = new List<byte[]>();

                lines.ForEach(p => {
                    byte[] newValues = new byte[p.Length + 1];
                    newValues[0] = 175;                                // set the prepended value
                    Array.Copy(p, 0, newValues, 1, p.Length);
                    if (newValues.Length == 10 && newValues[1] == 250)
                        newLines.Add(newValues);
                });
                







                newLines.ForEach(data =>
                {
                    if (data.Count() == 10)
                    {
                        TempData = new Data();
                        //var str = sr.ReadLine().Replace("\r\n", "").Replace(" ", "");
                        //str = str.Replace("\r", "").Replace("\n", "").Replace(" ", "");
                        //if (str.Length > 5)
                        {
                            //var data = StringToByteArray(str);
                            ExcelMode = true;
                            Analyze2(data.ToList());

                            ExcelData.Add(TempData);
                            ExcelMode = false;
                        }
                    }

                });

            });
        }
    }

    public class Data
    {
        public string RawData { get; set; }
        public string Temperature { get; set; }
        public string Roll { get; set; }
        public string Pitch { get; set; }
        public string Gate { get; set; }
        public string RecMode { get; set; }
        public string RecCommand { get; set; }
        public string Mode { get; set; }
        public string Command { get; set; }
        public string Counter1 { get; set; }
        public string Counter0 { get; set; }
        public string RPW { get; set; }
        public string Distance { get; set; }
        public string PacketCounter { get; set; }
        public string Pulse { get; set; }
        public string NumberOfTargets { get; set; }
        public string Speed { get; internal set; }
    }
}
