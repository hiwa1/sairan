﻿namespace Serial_Communication
    {
    partial class Form1
        {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
            {
            if (disposing && (components != null))
                {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
            {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbParity = new System.Windows.Forms.ComboBox();
            this.cmbBaudRate = new System.Windows.Forms.ComboBox();
            this.cmbPortName = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbStopBits = new System.Windows.Forms.ComboBox();
            this.cmbDataBits = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdHex = new System.Windows.Forms.RadioButton();
            this.rdText = new System.Windows.Forms.RadioButton();
            this.rtxtDataArea = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSend = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.lblDistance = new System.Windows.Forms.Label();
            this.lblRPW = new System.Windows.Forms.Label();
            this.lblCounter0 = new System.Windows.Forms.Label();
            this.lblMode = new System.Windows.Forms.Label();
            this.lblCommand = new System.Windows.Forms.Label();
            this.lblCounter1 = new System.Windows.Forms.Label();
            this.lblPitch = new System.Windows.Forms.Label();
            this.lblTemperature = new System.Windows.Forms.Label();
            this.lblPulse = new System.Windows.Forms.Label();
            this.lblNumberOfTarget = new System.Windows.Forms.Label();
            this.lblRecMode = new System.Windows.Forms.Label();
            this.lblRecCommand = new System.Windows.Forms.Label();
            this.lblGate = new System.Windows.Forms.Label();
            this.lblRoll = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.Pas = new System.Windows.Forms.Label();
            this.lblPacketNo = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btnSave = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.lblCorrect = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.lblIncorrect = new System.Windows.Forms.Label();
            this.chkCrc = new System.Windows.Forms.CheckBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblLastLine = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.lblReceivedBytes = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.lblSpeed = new System.Windows.Forms.Label();
            this.btnSaveAfter = new System.Windows.Forms.Button();
            this.btnReadBinaryFile = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cmbParity);
            this.groupBox1.Controls.Add(this.cmbBaudRate);
            this.groupBox1.Controls.Add(this.cmbPortName);
            this.groupBox1.Location = new System.Drawing.Point(12, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(212, 133);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "COM Serial Port Settings";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Parity:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Baud Rate:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "COM Port:";
            // 
            // cmbParity
            // 
            this.cmbParity.FormattingEnabled = true;
            this.cmbParity.Items.AddRange(new object[] {
            "None",
            "Even",
            "Odd"});
            this.cmbParity.Location = new System.Drawing.Point(75, 84);
            this.cmbParity.Name = "cmbParity";
            this.cmbParity.Size = new System.Drawing.Size(121, 21);
            this.cmbParity.TabIndex = 2;
            this.cmbParity.Text = "Select Parity";
            // 
            // cmbBaudRate
            // 
            this.cmbBaudRate.FormattingEnabled = true;
            this.cmbBaudRate.Items.AddRange(new object[] {
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200",
            "128000",
            "230400",
            "256000",
            "460800",
            "921600"});
            this.cmbBaudRate.Location = new System.Drawing.Point(75, 56);
            this.cmbBaudRate.Name = "cmbBaudRate";
            this.cmbBaudRate.Size = new System.Drawing.Size(121, 21);
            this.cmbBaudRate.TabIndex = 1;
            this.cmbBaudRate.Text = "Select Baude Rate";
            // 
            // cmbPortName
            // 
            this.cmbPortName.FormattingEnabled = true;
            this.cmbPortName.Location = new System.Drawing.Point(75, 25);
            this.cmbPortName.Name = "cmbPortName";
            this.cmbPortName.Size = new System.Drawing.Size(121, 21);
            this.cmbPortName.TabIndex = 0;
            this.cmbPortName.Text = "Select Port Name";
            this.cmbPortName.DropDown += new System.EventHandler(this.cmbPortName_DropDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 346);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Stop Bits:";
            this.label6.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 291);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Data Bits";
            this.label5.Visible = false;
            // 
            // cmbStopBits
            // 
            this.cmbStopBits.FormattingEnabled = true;
            this.cmbStopBits.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.cmbStopBits.Location = new System.Drawing.Point(7, 371);
            this.cmbStopBits.Name = "cmbStopBits";
            this.cmbStopBits.Size = new System.Drawing.Size(121, 21);
            this.cmbStopBits.TabIndex = 4;
            this.cmbStopBits.Text = "Select Stop Bits";
            this.cmbStopBits.Visible = false;
            // 
            // cmbDataBits
            // 
            this.cmbDataBits.FormattingEnabled = true;
            this.cmbDataBits.Items.AddRange(new object[] {
            "7",
            "8",
            "9"});
            this.cmbDataBits.Location = new System.Drawing.Point(7, 312);
            this.cmbDataBits.Name = "cmbDataBits";
            this.cmbDataBits.Size = new System.Drawing.Size(121, 21);
            this.cmbDataBits.TabIndex = 3;
            this.cmbDataBits.Text = "Select Data Bits";
            this.cmbDataBits.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdHex);
            this.groupBox2.Controls.Add(this.rdText);
            this.groupBox2.Location = new System.Drawing.Point(55, 149);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(106, 59);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data Mode";
            // 
            // rdHex
            // 
            this.rdHex.AutoSize = true;
            this.rdHex.Checked = true;
            this.rdHex.Location = new System.Drawing.Point(40, 36);
            this.rdHex.Name = "rdHex";
            this.rdHex.Size = new System.Drawing.Size(44, 17);
            this.rdHex.TabIndex = 1;
            this.rdHex.TabStop = true;
            this.rdHex.Text = "Hex";
            this.rdHex.UseVisualStyleBackColor = true;
            // 
            // rdText
            // 
            this.rdText.AutoSize = true;
            this.rdText.Location = new System.Drawing.Point(40, 17);
            this.rdText.Name = "rdText";
            this.rdText.Size = new System.Drawing.Size(46, 17);
            this.rdText.TabIndex = 0;
            this.rdText.Text = "Text";
            this.rdText.UseVisualStyleBackColor = true;
            // 
            // rtxtDataArea
            // 
            this.rtxtDataArea.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.rtxtDataArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtDataArea.Location = new System.Drawing.Point(244, 13);
            this.rtxtDataArea.Name = "rtxtDataArea";
            this.rtxtDataArea.ReadOnly = true;
            this.rtxtDataArea.Size = new System.Drawing.Size(339, 496);
            this.rtxtDataArea.TabIndex = 2;
            this.rtxtDataArea.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 436);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Send Data:";
            this.label1.Visible = false;
            // 
            // txtSend
            // 
            this.txtSend.Location = new System.Drawing.Point(8, 452);
            this.txtSend.Name = "txtSend";
            this.txtSend.Size = new System.Drawing.Size(230, 20);
            this.txtSend.TabIndex = 4;
            this.txtSend.Visible = false;
            // 
            // btnConnect
            // 
            this.btnConnect.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.Location = new System.Drawing.Point(55, 214);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(106, 47);
            this.btnConnect.TabIndex = 5;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = false;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnSend
            // 
            this.btnSend.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSend.Enabled = false;
            this.btnSend.Location = new System.Drawing.Point(12, 478);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 6;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = false;
            this.btnSend.Visible = false;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnClear.Location = new System.Drawing.Point(317, 538);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnRefresh.Location = new System.Drawing.Point(7, 399);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 10;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Visible = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // lblDistance
            // 
            this.lblDistance.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblDistance.Location = new System.Drawing.Point(695, 13);
            this.lblDistance.Name = "lblDistance";
            this.lblDistance.Size = new System.Drawing.Size(100, 23);
            this.lblDistance.TabIndex = 11;
            this.lblDistance.Text = "---";
            this.lblDistance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRPW
            // 
            this.lblRPW.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblRPW.Location = new System.Drawing.Point(695, 42);
            this.lblRPW.Name = "lblRPW";
            this.lblRPW.Size = new System.Drawing.Size(100, 23);
            this.lblRPW.TabIndex = 12;
            this.lblRPW.Text = "---";
            this.lblRPW.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCounter0
            // 
            this.lblCounter0.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblCounter0.Location = new System.Drawing.Point(695, 71);
            this.lblCounter0.Name = "lblCounter0";
            this.lblCounter0.Size = new System.Drawing.Size(100, 23);
            this.lblCounter0.TabIndex = 13;
            this.lblCounter0.Text = "---";
            this.lblCounter0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMode
            // 
            this.lblMode.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblMode.Location = new System.Drawing.Point(695, 159);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(100, 23);
            this.lblMode.TabIndex = 16;
            this.lblMode.Text = "---";
            this.lblMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCommand
            // 
            this.lblCommand.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblCommand.Location = new System.Drawing.Point(695, 130);
            this.lblCommand.Name = "lblCommand";
            this.lblCommand.Size = new System.Drawing.Size(100, 23);
            this.lblCommand.TabIndex = 15;
            this.lblCommand.Text = "---";
            this.lblCommand.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCounter1
            // 
            this.lblCounter1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblCounter1.Location = new System.Drawing.Point(695, 101);
            this.lblCounter1.Name = "lblCounter1";
            this.lblCounter1.Size = new System.Drawing.Size(100, 23);
            this.lblCounter1.TabIndex = 14;
            this.lblCounter1.Text = "---";
            this.lblCounter1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPitch
            // 
            this.lblPitch.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblPitch.Location = new System.Drawing.Point(695, 340);
            this.lblPitch.Name = "lblPitch";
            this.lblPitch.Size = new System.Drawing.Size(100, 23);
            this.lblPitch.TabIndex = 22;
            this.lblPitch.Text = "---";
            this.lblPitch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTemperature
            // 
            this.lblTemperature.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblTemperature.Location = new System.Drawing.Point(695, 310);
            this.lblTemperature.Name = "lblTemperature";
            this.lblTemperature.Size = new System.Drawing.Size(100, 23);
            this.lblTemperature.TabIndex = 21;
            this.lblTemperature.Text = "---";
            this.lblTemperature.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPulse
            // 
            this.lblPulse.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblPulse.Location = new System.Drawing.Point(695, 281);
            this.lblPulse.Name = "lblPulse";
            this.lblPulse.Size = new System.Drawing.Size(100, 23);
            this.lblPulse.TabIndex = 20;
            this.lblPulse.Text = "---";
            this.lblPulse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumberOfTarget
            // 
            this.lblNumberOfTarget.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblNumberOfTarget.Location = new System.Drawing.Point(695, 250);
            this.lblNumberOfTarget.Name = "lblNumberOfTarget";
            this.lblNumberOfTarget.Size = new System.Drawing.Size(100, 23);
            this.lblNumberOfTarget.TabIndex = 19;
            this.lblNumberOfTarget.Text = "---";
            this.lblNumberOfTarget.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRecMode
            // 
            this.lblRecMode.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblRecMode.Location = new System.Drawing.Point(695, 219);
            this.lblRecMode.Name = "lblRecMode";
            this.lblRecMode.Size = new System.Drawing.Size(100, 23);
            this.lblRecMode.TabIndex = 18;
            this.lblRecMode.Text = "---";
            this.lblRecMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRecCommand
            // 
            this.lblRecCommand.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblRecCommand.Location = new System.Drawing.Point(695, 189);
            this.lblRecCommand.Name = "lblRecCommand";
            this.lblRecCommand.Size = new System.Drawing.Size(100, 23);
            this.lblRecCommand.TabIndex = 17;
            this.lblRecCommand.Text = "---";
            this.lblRecCommand.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGate
            // 
            this.lblGate.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblGate.Location = new System.Drawing.Point(695, 399);
            this.lblGate.Name = "lblGate";
            this.lblGate.Size = new System.Drawing.Size(100, 23);
            this.lblGate.TabIndex = 24;
            this.lblGate.Text = "---";
            this.lblGate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRoll
            // 
            this.lblRoll.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblRoll.Location = new System.Drawing.Point(695, 369);
            this.lblRoll.Name = "lblRoll";
            this.lblRoll.Size = new System.Drawing.Size(100, 23);
            this.lblRoll.TabIndex = 23;
            this.lblRoll.Text = "---";
            this.lblRoll.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label8.Location = new System.Drawing.Point(589, 399);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 23);
            this.label8.TabIndex = 38;
            this.label8.Text = "Ex-Gate";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label9.Location = new System.Drawing.Point(589, 369);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 23);
            this.label9.TabIndex = 37;
            this.label9.Text = "Roll";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label10.Location = new System.Drawing.Point(589, 340);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 23);
            this.label10.TabIndex = 36;
            this.label10.Text = "Pitch";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label11.Location = new System.Drawing.Point(589, 310);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 23);
            this.label11.TabIndex = 35;
            this.label11.Text = "Temperature";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label12.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label12.Location = new System.Drawing.Point(589, 281);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 23);
            this.label12.TabIndex = 34;
            this.label12.Text = "Pulse";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label13.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label13.Location = new System.Drawing.Point(589, 250);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 23);
            this.label13.TabIndex = 33;
            this.label13.Text = "NumberOfTarget";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label14.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label14.Location = new System.Drawing.Point(589, 219);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 23);
            this.label14.TabIndex = 32;
            this.label14.Text = "RecMode";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label15.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label15.Location = new System.Drawing.Point(589, 189);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 23);
            this.label15.TabIndex = 31;
            this.label15.Text = "RecCommand";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label16.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label16.Location = new System.Drawing.Point(589, 159);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(100, 23);
            this.label16.TabIndex = 30;
            this.label16.Text = "Mode";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label17.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label17.Location = new System.Drawing.Point(589, 130);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(100, 23);
            this.label17.TabIndex = 29;
            this.label17.Text = "Command";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label18.Location = new System.Drawing.Point(589, 101);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(100, 23);
            this.label18.TabIndex = 28;
            this.label18.Text = "Counter1";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label19.Location = new System.Drawing.Point(589, 71);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(100, 23);
            this.label19.TabIndex = 27;
            this.label19.Text = "Counter0";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label20.Location = new System.Drawing.Point(589, 42);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(100, 23);
            this.label20.TabIndex = 26;
            this.label20.Text = "RPW";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label21.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label21.Location = new System.Drawing.Point(589, 13);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(100, 23);
            this.label21.TabIndex = 25;
            this.label21.Text = "Distance";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Pas
            // 
            this.Pas.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.Pas.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.Pas.Location = new System.Drawing.Point(589, 463);
            this.Pas.Name = "Pas";
            this.Pas.Size = new System.Drawing.Size(100, 23);
            this.Pas.TabIndex = 40;
            this.Pas.Text = "Packet No";
            this.Pas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPacketNo
            // 
            this.lblPacketNo.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblPacketNo.Location = new System.Drawing.Point(695, 463);
            this.lblPacketNo.Name = "lblPacketNo";
            this.lblPacketNo.Size = new System.Drawing.Size(100, 23);
            this.lblPacketNo.TabIndex = 39;
            this.lblPacketNo.Text = "---";
            this.lblPacketNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label22.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label22.Location = new System.Drawing.Point(802, 44);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(43, 23);
            this.label22.TabIndex = 42;
            this.label22.Text = "ns";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label23.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label23.Location = new System.Drawing.Point(802, 13);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(43, 23);
            this.label23.TabIndex = 41;
            this.label23.Text = "cm";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label24.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label24.Location = new System.Drawing.Point(802, 341);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(43, 23);
            this.label24.TabIndex = 44;
            this.label24.Text = "degree";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label25.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label25.Location = new System.Drawing.Point(802, 310);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(43, 23);
            this.label25.TabIndex = 43;
            this.label25.Text = "C";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label26.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label26.Location = new System.Drawing.Point(802, 399);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(43, 23);
            this.label26.TabIndex = 46;
            this.label26.Text = "m";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label27.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label27.Location = new System.Drawing.Point(802, 369);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 23);
            this.label27.TabIndex = 45;
            this.label27.Text = "degree";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSave.Location = new System.Drawing.Point(236, 539);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 47;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(398, 538);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(185, 23);
            this.button1.TabIndex = 48;
            this.button1.Text = "Loading File,SaveAs Excel";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label7.Location = new System.Drawing.Point(589, 530);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 23);
            this.label7.TabIndex = 50;
            this.label7.Text = "Correct Count";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCorrect
            // 
            this.lblCorrect.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblCorrect.Location = new System.Drawing.Point(695, 530);
            this.lblCorrect.Name = "lblCorrect";
            this.lblCorrect.Size = new System.Drawing.Size(100, 23);
            this.lblCorrect.TabIndex = 49;
            this.lblCorrect.Text = "---";
            this.lblCorrect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label29.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label29.Location = new System.Drawing.Point(589, 564);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(100, 23);
            this.label29.TabIndex = 52;
            this.label29.Text = "Incorrect Count";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblIncorrect
            // 
            this.lblIncorrect.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblIncorrect.Location = new System.Drawing.Point(695, 564);
            this.lblIncorrect.Name = "lblIncorrect";
            this.lblIncorrect.Size = new System.Drawing.Size(100, 23);
            this.lblIncorrect.TabIndex = 51;
            this.lblIncorrect.Text = "---";
            this.lblIncorrect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkCrc
            // 
            this.chkCrc.Location = new System.Drawing.Point(592, 594);
            this.chkCrc.Name = "chkCrc";
            this.chkCrc.Size = new System.Drawing.Size(97, 22);
            this.chkCrc.TabIndex = 53;
            this.chkCrc.Text = "Check CRC";
            this.chkCrc.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.Cornsilk;
            this.progressBar1.Location = new System.Drawing.Point(26, 617);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(803, 23);
            this.progressBar1.Step = 1;
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 54;
            this.progressBar1.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblLastLine
            // 
            this.lblLastLine.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblLastLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblLastLine.Location = new System.Drawing.Point(304, 512);
            this.lblLastLine.Name = "lblLastLine";
            this.lblLastLine.Size = new System.Drawing.Size(279, 23);
            this.lblLastLine.TabIndex = 55;
            this.lblLastLine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label28.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label28.Location = new System.Drawing.Point(589, 496);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(100, 23);
            this.label28.TabIndex = 58;
            this.label28.Text = "Received Bytes";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblReceivedBytes
            // 
            this.lblReceivedBytes.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblReceivedBytes.Location = new System.Drawing.Point(695, 496);
            this.lblReceivedBytes.Name = "lblReceivedBytes";
            this.lblReceivedBytes.Size = new System.Drawing.Size(100, 23);
            this.lblReceivedBytes.TabIndex = 57;
            this.lblReceivedBytes.Text = "---";
            this.lblReceivedBytes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label30.Location = new System.Drawing.Point(238, 511);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(60, 23);
            this.label30.TabIndex = 59;
            this.label30.Text = "Last Line";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.label31.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label31.Location = new System.Drawing.Point(589, 431);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(100, 23);
            this.label31.TabIndex = 61;
            this.label31.Text = "Speed";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSpeed
            // 
            this.lblSpeed.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lblSpeed.Location = new System.Drawing.Point(695, 431);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(100, 23);
            this.lblSpeed.TabIndex = 60;
            this.lblSpeed.Text = "---";
            this.lblSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSaveAfter
            // 
            this.btnSaveAfter.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSaveAfter.Location = new System.Drawing.Point(236, 568);
            this.btnSaveAfter.Name = "btnSaveAfter";
            this.btnSaveAfter.Size = new System.Drawing.Size(75, 23);
            this.btnSaveAfter.TabIndex = 62;
            this.btnSaveAfter.Text = "Save after";
            this.btnSaveAfter.UseVisualStyleBackColor = false;
            this.btnSaveAfter.Click += new System.EventHandler(this.btnSaveAfter_Click);
            // 
            // btnReadBinaryFile
            // 
            this.btnReadBinaryFile.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnReadBinaryFile.Location = new System.Drawing.Point(398, 568);
            this.btnReadBinaryFile.Name = "btnReadBinaryFile";
            this.btnReadBinaryFile.Size = new System.Drawing.Size(185, 23);
            this.btnReadBinaryFile.TabIndex = 63;
            this.btnReadBinaryFile.Text = "Loading Binary File,SaveAs Excel";
            this.btnReadBinaryFile.UseVisualStyleBackColor = false;
            this.btnReadBinaryFile.Click += new System.EventHandler(this.btnReadBinaryFile_ClickAsync);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(857, 646);
            this.Controls.Add(this.btnReadBinaryFile);
            this.Controls.Add(this.btnSaveAfter);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.lblSpeed);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.lblReceivedBytes);
            this.Controls.Add(this.lblLastLine);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.chkCrc);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.lblIncorrect);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblCorrect);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.cmbStopBits);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.cmbDataBits);
            this.Controls.Add(this.Pas);
            this.Controls.Add(this.lblPacketNo);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.lblGate);
            this.Controls.Add(this.lblRoll);
            this.Controls.Add(this.lblPitch);
            this.Controls.Add(this.lblTemperature);
            this.Controls.Add(this.lblPulse);
            this.Controls.Add(this.lblNumberOfTarget);
            this.Controls.Add(this.lblRecMode);
            this.Controls.Add(this.lblRecCommand);
            this.Controls.Add(this.lblMode);
            this.Controls.Add(this.lblCommand);
            this.Controls.Add(this.lblCounter1);
            this.Controls.Add(this.lblCounter0);
            this.Controls.Add(this.lblRPW);
            this.Controls.Add(this.lblDistance);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.txtSend);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rtxtDataArea);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Serial Port Interface";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

            }

        #endregion

        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbStopBits;
        private System.Windows.Forms.ComboBox cmbDataBits;
        private System.Windows.Forms.ComboBox cmbParity;
        private System.Windows.Forms.ComboBox cmbBaudRate;
        private System.Windows.Forms.ComboBox cmbPortName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox rtxtDataArea;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSend;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.RadioButton rdHex;
        private System.Windows.Forms.RadioButton rdText;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Label lblDistance;
        private System.Windows.Forms.Label lblRPW;
        private System.Windows.Forms.Label lblCounter0;
        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.Label lblCommand;
        private System.Windows.Forms.Label lblCounter1;
        private System.Windows.Forms.Label lblPitch;
        private System.Windows.Forms.Label lblTemperature;
        private System.Windows.Forms.Label lblPulse;
        private System.Windows.Forms.Label lblNumberOfTarget;
        private System.Windows.Forms.Label lblRecMode;
        private System.Windows.Forms.Label lblRecCommand;
        private System.Windows.Forms.Label lblGate;
        private System.Windows.Forms.Label lblRoll;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label Pas;
        private System.Windows.Forms.Label lblPacketNo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblCorrect;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label lblIncorrect;
        private System.Windows.Forms.CheckBox chkCrc;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblLastLine;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label lblReceivedBytes;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label lblSpeed;
        private System.Windows.Forms.Button btnSaveAfter;
        private System.Windows.Forms.Button btnReadBinaryFile;
    }
    }

